EESchema Schematic File Version 2
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:cmos4000
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intel
LIBS:interface
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:linear
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:modules
LIBS:motor_drivers
LIBS:motorola
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:Power_Management
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:zetex
LIBS:Zilog
LIBS:PCB-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP1642 U?
U 1 1 5B2642CF
P 2050 1000
F 0 "U?" H 2100 1100 60  0000 C CNN
F 1 "MCP1642" H 2300 600 60  0000 C CNN
F 2 "" H 2050 1000 60  0001 C CNN
F 3 "" H 2050 1000 60  0001 C CNN
	1    2050 1000
	1    0    0    -1  
$EndComp
$Comp
L Battery_Cell BT?
U 1 1 5B264467
P 6500 2150
F 0 "BT?" H 6600 2250 50  0000 L CNN
F 1 "Battery_Cell" H 6600 2150 50  0000 L CNN
F 2 "" V 6500 2210 50  0001 C CNN
F 3 "" V 6500 2210 50  0001 C CNN
	1    6500 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B264522
P 6500 2250
F 0 "#PWR?" H 6500 2000 50  0001 C CNN
F 1 "GND" H 6500 2100 50  0000 C CNN
F 2 "" H 6500 2250 50  0001 C CNN
F 3 "" H 6500 2250 50  0001 C CNN
	1    6500 2250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B264591
P 3500 1200
F 0 "#PWR?" H 3500 950 50  0001 C CNN
F 1 "GND" H 3500 1050 50  0000 C CNN
F 2 "" H 3500 1200 50  0001 C CNN
F 3 "" H 3500 1200 50  0001 C CNN
	1    3500 1200
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST SW?
U 1 1 5B260323
P 6050 1650
F 0 "SW?" H 6050 1775 50  0000 C CNN
F 1 "SW_SPST" H 6050 1550 50  0000 C CNN
F 2 "" H 6050 1650 50  0001 C CNN
F 3 "" H 6050 1650 50  0001 C CNN
	1    6050 1650
	1    0    0    -1  
$EndComp
$Comp
L L L?
U 1 1 5B2604DD
P 2950 1300
F 0 "L?" V 2900 1300 50  0000 C CNN
F 1 "L" V 3025 1300 50  0000 C CNN
F 2 "" H 2950 1300 50  0001 C CNN
F 3 "" H 2950 1300 50  0001 C CNN
	1    2950 1300
	0    1    1    0   
$EndComp
$Comp
L C C?
U 1 1 5B260541
P 4300 1800
F 0 "C?" H 4325 1900 50  0000 L CNN
F 1 "C" H 4325 1700 50  0000 L CNN
F 2 "" H 4338 1650 50  0001 C CNN
F 3 "" H 4300 1800 50  0001 C CNN
	1    4300 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B260585
P 4300 1950
F 0 "#PWR?" H 4300 1700 50  0001 C CNN
F 1 "GND" H 4300 1800 50  0000 C CNN
F 2 "" H 4300 1950 50  0001 C CNN
F 3 "" H 4300 1950 50  0001 C CNN
	1    4300 1950
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5B2605A9
P 1500 1450
F 0 "C?" H 1525 1550 50  0000 L CNN
F 1 "C" H 1525 1350 50  0000 L CNN
F 2 "" H 1538 1300 50  0001 C CNN
F 3 "" H 1500 1450 50  0001 C CNN
	1    1500 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B2605DD
P 1500 1600
F 0 "#PWR?" H 1500 1350 50  0001 C CNN
F 1 "GND" H 1500 1450 50  0000 C CNN
F 2 "" H 1500 1600 50  0001 C CNN
F 3 "" H 1500 1600 50  0001 C CNN
	1    1500 1600
	1    0    0    -1  
$EndComp
$Comp
L MCP73811T-420I/OT U?
U 1 1 5B260787
P 2350 3200
F 0 "U?" H 2100 3450 50  0000 C CNN
F 1 "MCP73811T-420I/OT" H 2350 2950 50  0000 C CNN
F 2 "" H 2350 3200 50  0001 C CNN
F 3 "" H 2350 3200 50  0001 C CNN
	1    2350 3200
	1    0    0    -1  
$EndComp
$Comp
L USB_OTG J?
U 1 1 5B2608CA
P 1150 2950
F 0 "J?" H 950 3400 50  0000 L CNN
F 1 "USB_OTG" H 950 3300 50  0000 L CNN
F 2 "" H 1300 2900 50  0001 C CNN
F 3 "" H 1300 2900 50  0001 C CNN
	1    1150 2950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B260966
P 950 3500
F 0 "#PWR?" H 950 3250 50  0001 C CNN
F 1 "GND" H 950 3350 50  0000 C CNN
F 2 "" H 950 3500 50  0001 C CNN
F 3 "" H 950 3500 50  0001 C CNN
	1    950  3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B260AB0
P 1750 3300
F 0 "#PWR?" H 1750 3050 50  0001 C CNN
F 1 "GND" H 1750 3150 50  0000 C CNN
F 2 "" H 1750 3300 50  0001 C CNN
F 3 "" H 1750 3300 50  0001 C CNN
	1    1750 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1300 1850 1300
Wire Wire Line
	2800 1000 4250 1000
Wire Wire Line
	2800 1100 3250 1100
Wire Wire Line
	2800 1200 3500 1200
Wire Wire Line
	3250 1100 3250 1200
Connection ~ 3250 1200
Wire Wire Line
	6500 1950 6500 1650
Wire Wire Line
	6500 1650 6250 1650
Wire Wire Line
	3200 1650 5850 1650
Wire Wire Line
	4250 1000 4250 1650
Wire Wire Line
	3100 1300 3200 1300
Wire Wire Line
	3200 1300 3200 1650
Connection ~ 4250 1650
Wire Wire Line
	2750 2750 2750 3300
Wire Wire Line
	1450 2750 2750 2750
Wire Wire Line
	1750 2750 1750 3100
Wire Wire Line
	1750 3100 1950 3100
Connection ~ 2750 3100
Wire Wire Line
	950  3350 1150 3350
Wire Wire Line
	950  3350 950  3500
Connection ~ 1050 3350
Connection ~ 1850 3100
Connection ~ 1750 2750
Wire Wire Line
	1950 3200 1750 3200
Wire Wire Line
	1750 3200 1750 3300
Text Label 6450 1650 0    60   ~ 0
Batt+ve
Wire Wire Line
	1850 1000 1800 1000
Wire Wire Line
	1800 1000 1800 850 
Wire Wire Line
	1800 850  2800 850 
Wire Wire Line
	2800 850  2800 1000
Wire Wire Line
	1950 3300 1900 3300
Wire Wire Line
	1900 3300 1900 3700
Wire Wire Line
	1900 3700 1400 3700
Text Label 1400 3700 0    60   ~ 0
Batt+ve
$Comp
L C C?
U 1 1 5B260D68
P 2050 2600
F 0 "C?" H 2075 2700 50  0000 L CNN
F 1 "C" H 2075 2500 50  0000 L CNN
F 2 "" H 2088 2450 50  0001 C CNN
F 3 "" H 2050 2600 50  0001 C CNN
	1    2050 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B260DBD
P 2050 2400
F 0 "#PWR?" H 2050 2150 50  0001 C CNN
F 1 "GND" H 2050 2250 50  0000 C CNN
F 2 "" H 2050 2400 50  0001 C CNN
F 3 "" H 2050 2400 50  0001 C CNN
	1    2050 2400
	-1   0    0    1   
$EndComp
$Comp
L C C?
U 1 1 5B260DE8
P 1850 3850
F 0 "C?" H 1875 3950 50  0000 L CNN
F 1 "C" H 1875 3750 50  0000 L CNN
F 2 "" H 1888 3700 50  0001 C CNN
F 3 "" H 1850 3850 50  0001 C CNN
	1    1850 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5B260E40
P 1850 4000
F 0 "#PWR?" H 1850 3750 50  0001 C CNN
F 1 "GND" H 1850 3850 50  0000 C CNN
F 2 "" H 1850 4000 50  0001 C CNN
F 3 "" H 1850 4000 50  0001 C CNN
	1    1850 4000
	1    0    0    -1  
$EndComp
Text HLabel 5800 1900 0    60   Input ~ 0
BATT+VE
Wire Wire Line
	6500 1900 5800 1900
Connection ~ 6500 1900
Connection ~ 1500 1300
$Comp
L +5V #PWR?
U 1 1 5B26122D
P 1150 1300
F 0 "#PWR?" H 1150 1150 50  0001 C CNN
F 1 "+5V" H 1150 1440 50  0000 C CNN
F 2 "" H 1150 1300 50  0001 C CNN
F 3 "" H 1150 1300 50  0001 C CNN
	1    1150 1300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
