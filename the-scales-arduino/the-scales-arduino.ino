#include <SPI.h>

#include <SD.h>

#include "HX711.h"

#include "U8glib.h"
//#include <avr/io.h>
//#include <avr/interrupt.h>



#define PIN_CLK 2
#define PIN_DT 3
#define PIN_SW 4
#define LED_PIN 5

#define MAXRECIPES 10
#define MAXSTRINGLENGTH 30
#define MENU_SCROLL_DELAY 1000 // in millis

U8GLIB_SSD1306_128X32 u8g(U8G_I2C_OPT_NONE);  // I2C / TWI

HX711 scale(A1, A0);    // parameter "gain" is ommited; the default value 128 is used by the library

enum State {
  S_RecipeSelect = 0, // Cycle through recipes showing name and cooking time and Servings
  S_Servings,         // Select number of servings
  S_Ingredients,      // List ingredients needed. And can weigh them so you can see if you have enough
  S_CookingInstructions,  // Instructions on how to cook it
};

struct Recipe_meta {
  char title[MAXSTRINGLENGTH + 1];
  char servingType[MAXSTRINGLENGTH + 1];
  unsigned int servings;
  unsigned int cookingTime;
};

struct Ingredient {
  char title[MAXSTRINGLENGTH + 1];
  char unit[MAXSTRINGLENGTH + 1];
  unsigned int amount;
};
    
//unsigned long timeStart = 0;
char string1[30];
//char list[(MAXRECIPES * (NAMELENGTH + 1))]; // max of ten files just while we're figuring out what the fuck a scale is
Recipe_meta currentRecipe;
Ingredient ingredient;
int numRecipes = 0;
int servings = 0;
unsigned int ingredientStartIndex = 0;

float weight = 0;
char weightS[6];

enum State state = S_RecipeSelect;

volatile int encoderPos = 1000; // "We're think of a better solution eventually - Cam"
volatile bool dt;
volatile bool clk;
volatile int pos = 500;
volatile bool newPos = true;
unsigned long newPosTime;
unsigned long last_sw_press = 0;

void setup() {
  // put your setup code here, to run once:
  u8g.setFont(u8g_font_helvR12);

  scale.set_scale(435.2f);                      // this value is obtained by calibrating the scale with known weights; see the README for details
  scale.tare();
  
  Serial.begin(115200);

  sdInit();
  
  
  pinMode(PIN_DT, INPUT);
  pinMode(PIN_CLK, INPUT);
  pinMode(PIN_SW, INPUT_PULLUP);
  dt = digitalRead(PIN_DT);
  clk = digitalRead(PIN_CLK);
  
  attachInterrupt(digitalPinToInterrupt(PIN_DT), clkISR, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_CLK), clkISR, CHANGE);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED_PIN, digitalRead(PIN_SW));
  switch(state) {
    case S_RecipeSelect:
      // Update Recipe meta if needed
      if (newPos) {
        newPos = false;
        newPosTime = millis();
        readMetaData(pos%numRecipes);
        printCurrentRecipeSerial();
      }

      if (switch_press()) {
        Serial.println(F("Moving to the serving state"));
        state = S_Servings;
        servings = currentRecipe.servings;
        ingredientStartIndex = scanForIngredients(); // Change this later to have a function which does it
        pos = 0;
        encoderPos = 0;
      }
      break;
    case S_Servings:
      // Scale the servings yo;
        servings = currentRecipe.servings + pos;
        
        if (switch_press()) {
          state = S_Ingredients;
        }
      break;

    case S_Ingredients:
      // Frist we need to load all of them, and figure out roughly how many we want.
    default:
      Serial.println("Default case");
      break;
  }

  // Display what should be displayed
  u8g.firstPage();
  do {
    draw();
    //u8g.setColorIndex(1);
  } while ( u8g.nextPage() );
  


    /*
  //char tempString[8];
  dtostrf(weight, 1, 0, string1);
  //Serial.println(string1);
  sprintf(string1, "%s, %04d",string1, (millis() - timeStart));
  timeStart = millis();
  //Serial.println(string1);

  
  if (state == S_Instructions) {
    weight = scale.get_units(3);
    if (-1 < weight && weight < 0) {
      weight = 0;
    }
  }
  
 
  u8g.firstPage();
  do {
    draw();
    //u8g.setColorIndex(1);
  } while ( u8g.nextPage() );
  
  digitalWrite(LED_PIN, digitalRead(PIN_SW));
  if (!digitalRead(PIN_SW)) {
    // Button push
    readMetaData((pos % numRecipes));    
  }
  */
}

void draw(void) {
  
  switch(state) {
    
    case S_RecipeSelect:
    
      if (newPosTime + MENU_SCROLL_DELAY >= millis() || strlen(currentRecipe.title) < 13) {
        // dont scroll
        u8g.drawStr(0, 12, currentRecipe.title);
        strcpy(string1, "Min: "); // I only need to do this once, unless we're using string1 elsewhere...
        itoa(currentRecipe.cookingTime, &(string1[5]), 10);
        //sprintf(string1, "Min: %s", string1);
        u8g.drawStr(0, 30, string1); 
      } else {
        // scroll
        int offset = -((int) (millis() - newPosTime) - MENU_SCROLL_DELAY)/60;
        u8g.drawStr(offset, 12, currentRecipe.title);
        u8g.drawStr(0, 30, string1); 
      }
      break;
     
    case S_Servings:
      // Display the type of serving and the modified number
      u8g.drawStr(0, 12, currentRecipe.servingType);
      itoa(currentRecipe.servings, string1, 10);
      strcpy(&string1[2],  " -> ");
      itoa(servings, &(string1[6]), 10);
      u8g.drawStr(0,30, string1);
      
      break;
    case S_Ingredients:

      break;
    default:

      break;
  }
  
}

bool switch_press() {
  if (!digitalRead(PIN_SW) &&  millis() - last_sw_press > 600) {//last_sw_press + 600 > millis()) {
    last_sw_press = millis();
    return true;
  } else {
    return false;
  }
}

void clkISR(void) {
  bool dt_new = digitalRead(PIN_DT);
  bool clk_new = digitalRead(PIN_CLK);
  
  if (clk && dt) {
    if (!clk_new && dt_new) {
      encoderPos++;
    } else if (clk_new && !dt_new) {
      encoderPos--;
    }
  } else if (clk && !dt) {
    if (clk_new && dt_new) {
        encoderPos++;
      } else if (!clk_new && !dt_new) {
        encoderPos--;
      }
  } else if (!clk && dt) {
    if (!clk_new && !dt_new) {
        encoderPos++;
      } else if (clk_new && dt_new) {
        encoderPos--;
      }
  } else if (!clk && !dt) {
    if (clk_new && !dt_new) {
        encoderPos++;
      } else if (!clk_new && dt_new) {
        encoderPos--;
      }
  }
  if ((dt != dt_new || clk != clk_new) && dt_new == clk_new) {
    newPos = true;
    pos = encoderPos / 2;
  }
  dt = dt_new;
  clk = clk_new;
}


