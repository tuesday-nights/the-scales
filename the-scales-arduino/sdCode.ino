
File dir;


void getNumRecipes(File dir) {
  int i = 0;
  while (i < MAXRECIPES) {
    File entry = dir.openNextFile();
    if (!entry) {
      numRecipes = i;
      dir.rewindDirectory();
      break;
    } else if (!entry.isDirectory()) {
      i++;
    }
    entry.close();
  }
}

int scanForIngredients(void) {
  
  File entry = SD.open(currentRecipe.title);
  String t = entry.readStringUntil('\n');
  int index = 0;
  while (!t.equals("[ingredients]")) {
    t = entry.readStringUntil('\n');
    index++;
  }
  return index;
}

void readIngredient(int ingIndex) {
  // Open the file
  File entry = SD.open(currentRecipe.title);

  // Read up until the nth ingredient
  for (int i = 0; i<ingredientStartIndex + 3*ingIndex; i++) {
    entry.readStringUntil('\n');
  }
  Serial.println(entry.readStringUntil('\n'));
  entry.readStringUntil('\n');
  entry.readStringUntil('\n');
}


void readMetaData(int numFile) {
  //Serial.println(numFile);
  int i = 0;
  File entry;
  while (i <= numFile) {
    entry = dir.openNextFile();
    if (!entry) {
      Serial.print("Could not find file number: ");
      Serial.println(numFile);
      break;
    } else if (!entry.isDirectory()) {
      // Only count files. Not folders.
      if (i == numFile) {
        // This is the file I want
        actuallyReadMetaData(entry);
        break;
      }
      i++;
    }
    entry.close();
  }
  entry.close();
  dir.rewindDirectory();
}



bool actuallyReadMetaData(File entry) {
  // Returns 1-1 if successfully does the thing
  // 1 otherwise
  // Exit if not a recipe file
  String t = entry.readStringUntil('\n');
  if (!t.equals("[recipe_meta]")) {
    Serial.println("Not a recipe file");
    Serial.println(t);
    return 1;
  }

  // Read name
  t = entry.readStringUntil('\n');
  t.toCharArray(currentRecipe.title, MAXSTRINGLENGTH + 1);

  // Read Cooking time
  t = entry.readStringUntil('\n');
  currentRecipe.cookingTime = t.toInt();

  // Read Serving Type
  t = entry.readStringUntil('\n');
  t.toCharArray(currentRecipe.servingType, MAXSTRINGLENGTH + 1);

  // Read Serving number
  t = entry.readStringUntil('\n');
  currentRecipe.servings = t.toInt();
}

void printCurrentRecipeSerial(void) {
  Serial.print("Title :");
  Serial.println(currentRecipe.title);
  Serial.print("Time  :");
  Serial.println(currentRecipe.cookingTime);
  Serial.print("Type  :");
  Serial.println(currentRecipe.servingType);
  Serial.print("Num   :");
  Serial.println(currentRecipe.servings);
  Serial.println();
}


void sdInit(void) {
  if (SD.begin()) {
    Serial.println("SD Init");

    dir = SD.open("/");
    getNumRecipes(dir);
    Serial.print("Num recipes: ");
    Serial.println(numRecipes);
  }
}




